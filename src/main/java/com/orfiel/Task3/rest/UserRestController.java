package com.orfiel.Task3.rest;

import com.orfiel.Task3.data.UserRepo;
import com.orfiel.Task3.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserRestController {

    @Autowired
    private UserRepo userRepo;

    @GetMapping("/admin/list")
    public List<User> listAllUser() {
        return userRepo.findAll();
    }

    @GetMapping("/admin/{username:[a-zA-Z]+}")
    public User getUser(@PathVariable("username") String username) {
        return userRepo.findByUsername(username);
    }
    @GetMapping("/{username:[a-zA-Z]+}")
    public User getOrdinaryUser(@PathVariable("username") String username) {
        return userRepo.findByUsername(username);
    }

    @GetMapping("/admin/{id:[0-9]+}")
    public User getUser(@PathVariable("id") Long id) {
        return userRepo.findOne(id);

    }

    @PostMapping("/admin/add")
    public String add(@Valid @RequestBody User user, BindingResult result){
        if(result.hasErrors()){
            return "redirect:/error";
        }
        if(userRepo.findByUsername(user.getUsername()) != null){
            return "redirect:/isnieje";
        }
        userRepo.save(user);
        return "redirect:/dodano";
    }
    @PutMapping("/admin/update/{id}")
    public User updateUser(@PathVariable("id") Long userId, @Valid @RequestBody User user) {
        User updated = userRepo.findOne(userId);
        if(updated == null){
            return user;

        }
        update(updated, user);
        return user;
    }
    private void update(final User original, final User user) {

        if (user.getUsername() != null) {
            original.setUsername(user.getUsername());
        }if (user.getPassword() != null) {
            original.setPassword(user.getPassword());
        }if (user.getRole() != null) {
            original.setRole(user.getRole());
        }
        userRepo.save(original);
    }
    @GetMapping("/admin/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        userRepo.delete(id);
        return "redirect:/usunietys";

    }
}
