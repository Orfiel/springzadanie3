//package com.orfiel.Task3.web;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/css/**", "/public/**, ").permitAll()
//                .antMatchers("/api/user/").hasRole("USER")
//                .antMatchers("/api/user/admin/**").hasRole("ADMIN")
//                .and().csrf().disable()
//                .httpBasic();
//    }
//
//    @Autowired
//    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .inMemoryAuthentication()
//                .withUser("Adam").password("adam").roles("ADMIN").and()
//                .withUser("Paul").password("pau").roles("ADMIN").and()
//                .withUser("Wojtek").password("woj").roles("USER").and()
//                .withUser("Robert").password("rob").roles("USER");
//
//
//
//
//    }
//
//
//
//
//
//}
